let formSubmit = document.querySelector("#createCourse")

formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()

	//get the value of #courseName and assign it to the courseName variable
	let courseName = document.querySelector("#courseName").value
	//get the value of #courseDescription and assign it to the description variable
	let description = document.querySelector("#courseDescription").value
	//get the value of #price and assign it to the price variable
	let price = document.querySelector("#coursePrice").value
	//retrieve the JSON Web Token stored in our local storage for authentication
	let token = localStorage.getItem('token')

	fetch('https://ladyns-capstone.herokuapp.com/api/courses', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName,
			description: description,
			price: price
		})
	})
	.then(res => {
		return res.json()
	})
	.then(data => {
		if(data === true){
			//if creation of new course is successful, redirect to courses page
			window.location.replace('./courses.html')
		}else{
			alert('Something went wrong')
		}
	})
})
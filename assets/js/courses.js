let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector('#adminButton')
let cardFooter;

if(adminUser == "false" || !adminUser){
	modalButton.innerHTML = null
}else{
	modalButton.innerHTML = `<div class="col-md-2 offset-md-10">
	<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
</div>`
}

//fetch the courses from our API
fetch('https://ladyns-capstone.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {
	// console.log(data)
	let courseData;

	//if the number of courses fetched is less than 1, display no courses available.
	if(data.length < 1){
		courseData = "No courses available."
	}else{
		//else iterate the courses collection and display each course
		courseData = data.map(course => {
			// console.log(course._id)
			if(adminUser == "false" || !adminUser){
				//check if the user is not an admin
				//if not an admin, display the select course button
				cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block selectButton">Select Course</a>`
			}else{
				//if user is an admin, display the edit course button
				cardFooter = `<a href="./enrolleesCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block enrolleesButton">Enrollees</a>

				<a href="./viewCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block viewButton">View Course</a>

				<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton">Edit</a>`

			}

			return (
					`<div class="col-md-6 my-3">
					<div class="card">
					<img src="https://i.telegraph.co.uk/multimedia/archive/02758/brainy_2758840b.jpg" class="proj-image card-img-top img-fluid">
					<div class="card-body">
						<h5 class="card-title"><a href="http://localhost:3000/api/courses/">${course.name}</a></h5>
						<p class="card-text text-left">${course.description}</p>
						<p class="card-text text-right">₱ ${course.price}</p>
					</div>

					<div class="card-footer">
						${cardFooter}
					</div>
				</div>
			</div>`
					)

		}).join("")
		//since the collection is an array, we can use the join method to indicate the separator of each element. We replaced the commas with an empty strings to remove them.
	}

	let container = document.querySelector('#coursesContainer')
	container.innerHTML = courseData
})
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId')
let deleteButton = document.querySelector('#deleteButton')
let token = localStorage.getItem('token')

    deleteButton.addEventListener("click", (e) => {
        e.preventDefault()
    

        fetch(`https://ladyns-capstone.herokuapp.com/api/courses/${courseId}`, {
        method: "DELETE",
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },

    })
    .then(res => {
        return res.json()
    })
    .then(data => {
        if(data === true) {
            // console.log(data)
            window.location.replace('./courses.html')
        }else {
            alert ('Something went wrong!')
        }
    })
    })
    


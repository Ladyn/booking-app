// instantiate a URLSearchParams object so we can execute methods to access 
// specific parts of the query string
let params = new URLSearchParams(window.location.search);

// get method returns the value of the key passed in as an argument

let courseId = params.get('courseId')

let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let description = document.querySelector("#courseDescription")

fetch(`https://ladyns-capstone.herokuapp.com/api/courses/${courseId}`)
.then(res => {
    return res.json()
})
.then(data => {
	// assign the current values as placeholders
	name.placeholder = data.name
	price.placeholder = data.price
	description.placeholder = data.description

	document.querySelector("#editCourse").addEventListener("submit", (e) => {
		e.preventDefault()

		let courseName = name.value
		let desc = description.value
		let priceValue = price.value

		let token = localStorage.getItem('token')

		fetch('https://ladyns-capstone.herokuapp.com/api/courses', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                courseId: courseId,
                name: courseName,
                description: desc,
                price: priceValue
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            //creation of new course successful
            if(data === true){
                //redirect to courses index page
                window.location.replace("./courses.html")
            }else{
                //error in creating course, redirect to error page
                alert("Something went wrong")
            }
        })
	})
})
let loginForm = document.querySelector("#logInUser")

loginForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value

	if(email === "" || password === ""){
		alert("Please input your email and/or password")
	}else{
		fetch('https://ladyns-capstone.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data.access){
				//set JWT in local storage
				localStorage.setItem('token', data.access)
				fetch('https://ladyns-capstone.herokuapp.com/api/users/details', {
					headers: {
						Authorization:  `Bearer ${data.access}`
					}
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					//set the global user state to have properties containing the authenticated user's ID and role
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					window.location.replace("./courses.html")
				})
			}
		})
	}
})
let params = new URLSearchParams(window.location.search);
let adminUser = localStorage.getItem('isAdmin');
let token = localStorage.getItem('token');
let userId = localStorage.getItem('userId');
let profileContainer = document.querySelector('#profileContainer');
let courseName = document.querySelector('#courseName')

let courseIds = []
let courseNames = []

fetch('https://ladyns-capstone.herokuapp.com/api/users/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
		// console.log(data.enrollments)
		// let courseName = document.querySelector('#courseName')>>>>
		
 		profileContainer.innerHTML = `<div class="container center">
	    <div class="card my-3" style="max-width:18rem">
	    	<img class="card-img-top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ1gD3Kd7xvC2o31Xu2KB7Rb1mksmaucS-EPD3jV1In6A&usqp=CAU&ec=45702844" style="width:100%">
	    <div class="card-body">
       		<h5 class="card-header text-center text-white bg-secondary">Course Enrolled ${data.courseName}</h5><br>
 				<h6 class="card-text text-center text-danger">Full Name: ${data.firstName} ${data.lastName}</h6>
 				<p class="card-text text-center text-danger">Email: ${data.email}</p>
 				<p class="card-text text-center text-danger">Mobile Number: ${data.mobileNo}</p>
     	</div>
   		</div>`

   		for(let i = 0; i < data.enrollments.length; i++){
		courseIds.push(data.enrollments[i].courseId)
	}

 		fetch('https://ladyns-capstone.herokuapp.com/api/courses')
 		.then(res => res.json())
 		.then(data => {
 			for(let i = 0; i < data.length; i++) {
 				// courseNames.push(data[i].name)
 				if(courseIds.includes(data[i]._id)) {
 					courseNames.push(data[i].name)
 				}
 			}
 		})	

 		let coursesList = courseNames.join("\n")
  	//Course names are here in coursesList
  	
	// let courseEnrolled = data.enrollments.map(course => {
	// 	courseIds.push(course.courseId)
	// })
	// console.log(courseIds)
})

// `<div class="container">
//   <div class="card" style="width:400px">
//     <img class="card-img-top" src="https://cdn3.vectorstock.com/i/thumb-large/63/42/avatar-photo-placeholder-icon-design-vector-30916342.jpg" alt="Card image" style="width:100%">
//     <div class="card-body">
//       <h4 class="card-title">John Doe</h4>
//       <p class="card-text">Some example text some example text. John Doe is an architect and engineer</p>
//     </div>
//   </div>`
let navItems =  document.querySelector("#navSession")

let userToken = localStorage.getItem("token")

if(!userToken){
	navItems.innerHTML = `<li class="nav-item">
	<a href="./login.html" class="nav-link">Log In</a>
</li>`
}else{
	navItems.innerHTML = `<ul class="navbar-nav ml-auto">
	<li class="nav-item">
	<a href="./profile.html" class="nav-link">Profile</a>
	</li>
	<li class="nav-item">
		<a href="./logout.html" class="nav-link">Log Out</a>
	</li>
	</ul>`
}

